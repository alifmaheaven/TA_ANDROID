package id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ajts.androidmads.library.SQLiteToExcel;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.RecyclerView.RecyclerViewAdapter;
import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.SQLite.DataHelper;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("StaticFieldLeak")
    public static MainActivity ma;

    protected Cursor cursor;
    String[] daftar;
    Menu menu;
    DataHelper MyDatabase;
    private ArrayList<String> noiventarisList;
    private ArrayList<String> namaasetList;
    private ArrayList<String> hargasaruanList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buat = findViewById(R.id.Buat);

        buat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Intent inte = new Intent(MainActivity.this, BuatInventaris.class);
                startActivity(inte);
                finish();
            }
        });

        Button cetak = findViewById(R.id.Cetak);
        cetak.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                String directory_path = Environment.getExternalStorageDirectory().getPath() + "/Backup/";
                File file = new File(directory_path);
                if (!file.exists()) {
                    file.mkdirs();
                }
                // Export SQLite DB as EXCEL FILE
                SQLiteToExcel sqliteToExcel = new SQLiteToExcel(getApplicationContext(), DataHelper.DATABASE_NAME, directory_path);
                sqliteToExcel.exportAllTables("users.xls", new SQLiteToExcel.ExportListener() {
                    @Override
                    public void onStart() {

                    }

                    @Override
                    public void onCompleted(String filePath) {
                        Toast.makeText(getApplicationContext(), "Berhasil tersimpan", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(Exception e) {

                    }
                });
            }

        });


        noiventarisList = new ArrayList<>();
        namaasetList = new ArrayList<>();
        hargasaruanList = new ArrayList<>();
        ma = this;

        MyDatabase = new DataHelper(getBaseContext());
        RecyclerView recyclerView = findViewById(R.id.recycler);

        RefreshList();


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        RecyclerView.Adapter adapter = new RecyclerViewAdapter(noiventarisList, namaasetList, hargasaruanList);
        //Memasang Adapter pada RecyclerView
        recyclerView.setAdapter(adapter);
        //Membuat Underline pada Setiap Item Didalam List
        DividerItemDecoration itemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        itemDecoration.setDrawable(Objects.requireNonNull(ContextCompat.getDrawable(getApplicationContext(), R.drawable.line)));
        recyclerView.addItemDecoration(itemDecoration);
    }


    public void RefreshList() {
        SQLiteDatabase db = MyDatabase.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM inventaris", null);

        cursor.moveToFirst();
        for (int count = 0; count < cursor.getCount(); count++) {

            cursor.moveToPosition(count);//Berpindah Posisi dari no index 0 hingga no index terakhir

            noiventarisList.add(cursor.getString(0));
            namaasetList.add(cursor.getString(1));
            hargasaruanList.add(cursor.getString(8));


        }


    }


}

