package id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.volley.SharedPrefManager;
import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.volley.URLs;
import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.volley.User;
import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.volley.VolleySingleton;

public class Login extends AppCompatActivity implements View.OnClickListener {

    TextInputEditText musername, mpassword;
    Button login;
    ProgressBar mprogressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }


        musername = findViewById(R.id.Username);
        mpassword = findViewById(R.id.Password);
        mprogressbar = findViewById(R.id.progressBar);
        login = findViewById(R.id.Login);
        login.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        if (login == v) {
            if (musername.length() == 0 && mpassword.length() == 0) {
                musername.setError("Mohon isi Username anda");
                mpassword.setError("Masukan Password anda");
                musername.requestFocus();
                mpassword.requestFocus();
            } else if (musername.length() == 0) {
                musername.setError("Mohon isi Username anda");
                musername.requestFocus();
            } else if (mpassword.length() == 0) {
                mpassword.setError("Masukan Password anda");
                musername.requestFocus();
            } else {
                login();
            }
        }

    }

    private void login() {

        final String username = musername.getText().toString();
        final String password = mpassword.getText().toString();


        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                String user = obj.getString("UserName");
                                String pass = obj.getString("Password");
                                Log.d("data", "onResponse: " + user + pass);
                                if (username.equals(user) && password.equals(pass)) {
                                    User userModel = new User(
                                            obj.getInt("IDUser"),
                                            obj.getString("Name"),
                                            obj.getString("UserName"),
                                            obj.getString("Password"),
                                            obj.getString("Branch")
                                    );
                                    SharedPrefManager.getInstance(getApplicationContext()).userLogin(userModel);
                                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
