package id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.SQLite.DataHelper;
import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.volley.SharedPrefManager;
import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.volley.User;

@SuppressWarnings("ALL")
public class BuatInventaris extends AppCompatActivity implements View.OnClickListener {

    protected Cursor cursor;
    DataHelper dbHelper;
    EditText namaaset, spesifikasi, namaruangan, noaset, hargasatuan, satuan;
    Spinner idkategori, idtype;
    private Button simpan, kembali, tanggalpengadaan;
    private DatePickerDialog formatKalender;
    private SimpleDateFormat formatTanggal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_inventaris);

        dbHelper = new DataHelper(this);


        formatTanggal = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        namaaset = findViewById(R.id.NamaAset);
        spesifikasi = findViewById(R.id.Spesifikasi);
        namaruangan = findViewById(R.id.NamaRuangan);
        idkategori = findViewById(R.id.IDKategori);
        idtype = findViewById(R.id.IDType);
        noaset = findViewById(R.id.NoAset);
        hargasatuan = findViewById(R.id.HargaSatuan);
        satuan = findViewById(R.id.Satuan);

        tanggalpengadaan = findViewById(R.id.TanggalPengadaan);
        tanggalpengadaan.setOnClickListener(this);

        simpan = findViewById(R.id.Simpan);
        kembali = findViewById(R.id.Kembali);

        simpan.setOnClickListener(this);
        kembali.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (tanggalpengadaan == v) {
            tanggal();
        }
        if (simpan == v) {
            simpan();
        }
        if (kembali == v) {
            kembali();
        }
    }

    private void kembali() {

        startActivity(new Intent(BuatInventaris.this, MainActivity.class));
        finish();

    }

    private void simpan() {

        final User user = SharedPrefManager.getInstance(this).getUser();

        String ns = namaaset.getText().toString().trim();
        String sp = spesifikasi.getText().toString().trim();
        String tp = tanggalpengadaan.getText().toString().trim();
        String nr = namaruangan.getText().toString().trim();
        String ik = idkategori.getSelectedItem().toString().trim();
        String it = idtype.getSelectedItem().toString().trim();
        int na = Integer.valueOf(noaset.getText().toString().trim());
        int hs = Integer.valueOf(hargasatuan.getText().toString().trim());
        String s = satuan.getText().toString().trim();

        String noinventaris = user.getBranch() + tp;

        // TODO Auto-generated method stub
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.execSQL("insert into inventaris(NoInventaris, NamaAset, Spesifikasi, TanggalPengadaan, NamaRuangan, IDKategori, IDType, NoAset, HargaSatuan, Satuan) values ('" +
                noinventaris + "','" +
                ns + "','" +
                sp + "','" +
                tp + "','" +
                nr + "','" +
                ik + "','" +
                it + "','" +
                na + "','" +
                hs + "','" +
                s + "')");
        Toast.makeText(getApplicationContext(), "Berhasil", Toast.LENGTH_LONG).show();
        MainActivity.ma.RefreshList();
        startActivity(new Intent(BuatInventaris.this, MainActivity.class));
        finish();
    }

    private void tanggal() {

        Calendar kalender_akhir = Calendar.getInstance();
        formatKalender = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar kalender_akhir = Calendar.getInstance();
                kalender_akhir.set(year, monthOfYear, dayOfMonth);
                int estimasi = kalender_akhir.get(Calendar.DAY_OF_YEAR);


                String hari = Integer.valueOf(estimasi).toString();
                /**
                 * Update TextView dengan tanggal yang kita pilih
                 */
                tanggalpengadaan.setText(formatTanggal.format(kalender_akhir.getTime()));


            }

        }, kalender_akhir.get(Calendar.YEAR), kalender_akhir.get(Calendar.MONTH), kalender_akhir.get(Calendar.DAY_OF_MONTH));

        /**
         * Tampilkan DatePicker dialog
         */
        formatKalender.show();


    }
}
