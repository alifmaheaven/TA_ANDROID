package id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import id.sch.smktelkom.xirpl1_25_mohammadalifmaheavenhidayatullah_android.SQLite.DataHelper;

public class LihatInventaris extends AppCompatActivity {

    protected Cursor cursor;
    DataHelper dbHelper;

    Button kembali, hapus, update;
    TextView noinventaris, namaaset, spesifikasi, tanggalpengadaan, namaruangan, idkategori, idtype, noaset, hargasatuan, satuan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_inventaris);

        dbHelper = new DataHelper(this);

        noinventaris = findViewById(R.id.NoInventaris);
        namaaset = findViewById(R.id.NamaAset);
        spesifikasi = findViewById(R.id.Spesifikasi);
        tanggalpengadaan = findViewById(R.id.TanggalPengadaan);
        namaruangan = findViewById(R.id.NamaRuangan);
        idkategori = findViewById(R.id.IDKategori);
        idtype = findViewById(R.id.IDType);
        noaset = findViewById(R.id.NoAset);
        hargasatuan = findViewById(R.id.HargaSatuan);
        satuan = findViewById(R.id.Satuan);

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM inventaris WHERE NoInventaris = '" +
                getIntent().getStringExtra("no") + "'", null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            noinventaris.setText(cursor.getString(0));
            namaaset.setText(cursor.getString(1));
            spesifikasi.setText(cursor.getString(2));
            tanggalpengadaan.setText(cursor.getString(3));
            namaruangan.setText(cursor.getString(4));
            idkategori.setText(cursor.getString(5));
            idtype.setText(cursor.getString(6));
            noaset.setText(cursor.getString(7));
            hargasatuan.setText(cursor.getString(8));
            satuan.setText(cursor.getString(9));
        }
        kembali = findViewById(R.id.Kembali);
        kembali.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                finish();
            }
        });

        hapus = findViewById(R.id.Hapus);
        hapus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                finish();
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("delete from inventaris where NoInventaris = '" + getIntent().getStringExtra("no") + "'");
                startActivity(new Intent(LihatInventaris.this, MainActivity.class));
                finish();
            }
        });

        update = findViewById(R.id.Update);
        update.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View arg0) {
                String noin = noinventaris.getText().toString();
                Intent i = new Intent(LihatInventaris.this, UpdateInventaris.class);
                i.putExtra("no", noin);
                startActivity(i);

            }
        });

    }
}
